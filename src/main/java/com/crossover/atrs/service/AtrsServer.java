package com.crossover.atrs.service;

import com.crossover.atrs.service.config.AtrsConfig;
import com.crossover.atrs.service.module.AtrsModule;
import com.cyngn.vertx.web.JsonUtil;
import com.cyngn.vertx.web.RestApi;
import com.cyngn.vertx.web.RouterTools;
import com.google.inject.Guice;
import com.google.inject.Inject;
import io.vertx.core.AbstractVerticle;
import io.vertx.core.Future;
import io.vertx.core.http.HttpServer;
import io.vertx.core.shareddata.LocalMap;
import io.vertx.ext.web.Router;
import io.vertx.ext.web.handler.BodyHandler;
import io.vertx.ext.web.handler.CorsHandler;
import io.vertx.ext.web.handler.StaticHandler;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.List;

/**
 * Created by spalanisamy on 07/08/16.
 */
public class AtrsServer extends AbstractVerticle {

    // Logging setup. Vertx startup related log goes here.
    private static final Logger logger = LoggerFactory.getLogger(AtrsServer.class);

    private HttpServer server;

    private AtrsModule module;

    private AtrsConfig config;

    @Inject
    private List<RestApi> apis;

    // local shared map, shared across all vert.x instances
    private LocalMap<String, Long> initializationMap;

    @Override
    public void start(Future<Void> future) {

        Router router = Router.router(vertx);


        config = JsonUtil.parseJsonToObject(vertx.getOrCreateContext().config().toString(), AtrsConfig.class);

        logger.error("Server Config "+config.getPort()+config.getUploadDirectory());

        BodyHandler bodyHandler = BodyHandler.create(config.getUploadDirectory());

        module = new AtrsModule(vertx, config, onReady -> {
            // Create Guice injector and initialize this Verticle instance.

            Guice.createInjector(module).injectMembers(this);
            RouterTools.registerRootHandlers(router, bodyHandler);
            for (RestApi api : apis) {
                api.init(router);
                api.outputApi(logger);
            }
            // Handler for static resources (css/js/html etc)
            router.route("/s/*").handler(StaticHandler.create("webroot"));

            router.route("/").handler(ctx -> { ctx.response().sendFile("webroot/index.html").end(); });

            server = createHttpServer(future, config.getPort(), router);
        });
    }

    /**
     * Local method to create vertx server.
     * @param future
     * @param port
     * @param router
     * @return
     */
    private HttpServer createHttpServer(Future<Void> future, int port, Router router) {
        return vertx.createHttpServer()
            .requestHandler(router::accept)
            .listen(port, result -> {
                if (result.succeeded()) {
                    future.complete();
                } else {
                    future.fail(result.cause());
                }
            });
    }
}
