package com.crossover.atrs.service.config;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;

/**
 * Created by spalanisamy on 07/08/16.
 */
@JsonIgnoreProperties(ignoreUnknown = true)
public class AtrsConfig {

    @JsonProperty
    private int port = -1;

    @JsonProperty("uploadDirectory")
    private String uploadDirectory;

    public int getPort() {
        return port;
    }

    public void setPort(int port) {
        this.port = port;
    }

    public String getUploadDirectory() {
        return uploadDirectory;
    }

    public void setUploadDirectory(String uploadDirectory) {
        this.uploadDirectory = uploadDirectory;
    }
}