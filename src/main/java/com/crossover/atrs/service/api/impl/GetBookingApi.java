package com.crossover.atrs.service.api.impl;

import com.crossover.atrs.service.api.AbstractBookFlightApi;
import com.crossover.atrs.service.api.AbstractGetBookingApi;
import com.crossover.atrs.service.types.BookFlightRequest;
import com.crossover.atrs.service.types.GetBookingRequest;
import com.crossover.atrs.service.types.Status;
import com.crossover.atrs.service.types.TicketInfo;
import com.crossover.atrs.storage.cassandra.dal.BookingDal;
import com.crossover.atrs.storage.cassandra.dal.RouteDal;
import com.crossover.atrs.storage.cassandra.table.Booking;
import com.crossover.atrs.storage.cassandra.udt.Ticket;
import com.cyngn.vertx.web.HttpHelper;
import com.google.inject.Inject;
import io.vertx.core.json.JsonObject;
import io.vertx.ext.web.RoutingContext;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

/**
 * Created by spalanisamy on 07/08/16.
 */
public class GetBookingApi extends AbstractGetBookingApi {

    private final static Logger logger = LoggerFactory.getLogger(GetBookingApi.class);

    @Inject
    private BookingDal bookingDal;

    @Override
    public void process(RoutingContext context, GetBookingRequest getBookingRequest) {
        bookingDal.get((res) -> {
            HttpHelper.processResponse(res.value, context.response());
        }, getBookingRequest.getBookingId());
    }
}