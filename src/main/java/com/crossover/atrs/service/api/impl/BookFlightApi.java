package com.crossover.atrs.service.api.impl;

import com.crossover.atrs.service.api.AbstractBookFlightApi;
import com.crossover.atrs.service.types.BookFlightRequest;
import com.crossover.atrs.service.types.Status;
import com.crossover.atrs.service.types.TicketInfo;
import com.crossover.atrs.storage.cassandra.dal.BookingDal;
import com.crossover.atrs.storage.cassandra.dal.RouteDal;
import com.crossover.atrs.storage.cassandra.table.Booking;
import com.crossover.atrs.storage.cassandra.udt.Ticket;
import com.cyngn.vertx.web.HttpHelper;
import com.google.inject.Inject;
import io.vertx.core.json.JsonObject;
import io.vertx.ext.web.RoutingContext;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

/**
 * Created by spalanisamy on 07/08/16.
 */
public class BookFlightApi extends AbstractBookFlightApi {

    private final static Logger logger = LoggerFactory.getLogger(BookFlightApi.class);

    @Inject
    private BookingDal bookingDal;

    @Inject
    private RouteDal routeDal;

    private static final String BOOKING_STATUS = "booking_status";

    @Override
    public void process(RoutingContext context, BookFlightRequest bookFlightRequest) {
        JsonObject result = new JsonObject();

        List<Ticket> tickets = new ArrayList<>();
        for(TicketInfo ticket : bookFlightRequest.getTicketInfo()) {
            Ticket t = new Ticket();
            t.setBookingId(bookFlightRequest.getBookingId());
            t.setTicketId(ticket.getTicketId());
            t.setSeatId(ticket.getSeatId());
            t.setFlightRouteId(ticket.getFlightRouteId());
            t.setFlightTripId(ticket.getFlightTripId());
            t.setMealPreference(ticket.getMealPreference().toString());
            tickets.add(t);
        }

        Booking booking = new Booking();
        booking.setBookingId(bookFlightRequest.getBookingId());
        booking.setBookingUserId(bookFlightRequest.getUserId());
        booking.setStatus(Status.CONFIRMED.toString());
        booking.setTickets(tickets);
        booking.setBookingTime(new Date());
        bookingDal.save(booking, (res) -> {
            result.put(BOOKING_STATUS, Status.CONFIRMED);
            HttpHelper.processResponse(result, context.response());
        });
    }
}