package com.crossover.atrs.service.api.impl;

import com.crossover.atrs.service.api.AbstractGetBookingApi;
import com.crossover.atrs.service.api.AbstractListBookingsApi;
import com.crossover.atrs.service.types.GetBookingRequest;
import com.crossover.atrs.storage.cassandra.dal.BookingDal;
import com.cyngn.vertx.web.HttpHelper;
import com.google.inject.Inject;
import io.vertx.ext.web.RoutingContext;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * Created by spalanisamy on 07/08/16.
 */
public class ListBookingsApi extends AbstractListBookingsApi {

    private final static Logger logger = LoggerFactory.getLogger(ListBookingsApi.class);

    @Inject
    private BookingDal bookingDal;

    @Override
    public void process(RoutingContext context) {
        bookingDal.getAll((res) -> {
            HttpHelper.processResponse(res.value, context.response());
        });
    }
}