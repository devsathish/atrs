package com.crossover.atrs.service.api.impl;

import com.crossover.atrs.service.api.AbstractCancelBookingApi;
import com.crossover.atrs.service.api.AbstractGetBookingApi;
import com.crossover.atrs.service.types.CancelBookingRequest;
import com.crossover.atrs.service.types.GetBookingRequest;
import com.crossover.atrs.service.types.Status;
import com.crossover.atrs.storage.cassandra.dal.BookingDal;
import com.crossover.atrs.storage.cassandra.table.Booking;
import com.cyngn.vertx.web.HttpHelper;
import com.google.inject.Inject;
import io.vertx.core.json.JsonObject;
import io.vertx.ext.web.RoutingContext;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * Created by spalanisamy on 07/08/16.
 */
public class CancelBookingApi extends AbstractCancelBookingApi {

    private final static Logger logger = LoggerFactory.getLogger(CancelBookingApi.class);

    private static final String CANCELLATION_STATUS = "cancel_status";

    @Inject
    private BookingDal bookingDal;

    @Override
    public void process(RoutingContext context, CancelBookingRequest cancelBookingRequest) {

        bookingDal.get((res) -> {
            Booking booking = res.value;
            booking.setStatus(Status.CANCELLED.toString());
            bookingDal.save(res.value, (cr) -> {
                JsonObject result = new JsonObject();
                result.put(CANCELLATION_STATUS , Status.CANCELLED);
                HttpHelper.processResponse(result, context.response());
            });
        }, cancelBookingRequest.getBookingId());
    }
}