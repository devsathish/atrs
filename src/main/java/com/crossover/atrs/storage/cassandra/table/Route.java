package com.crossover.atrs.storage.cassandra.table;

import com.datastax.driver.mapping.annotations.Column;
import com.datastax.driver.mapping.annotations.PartitionKey;
import com.datastax.driver.mapping.annotations.Table;
import com.fasterxml.jackson.annotation.JsonProperty;
import java.lang.Override;
import java.lang.String;

/**
 * GENERATED CODE DO NOT MODIFY - last updated: 2016-08-07T13:03:45.787Z
 * generated by exovert - https://github.com/cyngn/exovert
 *
 * Table class for Cassandra - route
 */
@Table(
    keyspace = "atrs",
    name = "route"
)
public class Route {
  @Column(name = "flight_route_id")
  @JsonProperty("flight_route_id")
  @PartitionKey(0)
  public String flightRouteId;

  @Column
  @JsonProperty
  public String destination;

  @Column(name = "flight_id")
  @JsonProperty("flight_id")
  public String flightId;

  @Column
  @JsonProperty
  public String source;

  @Column
  @JsonProperty
  public String status;

  public void setFlightRouteId(String flightRouteId) {
    this.flightRouteId = flightRouteId;
  }

  public String getFlightRouteId() {
    return flightRouteId;
  }

  public void setDestination(String destination) {
    this.destination = destination;
  }

  public String getDestination() {
    return destination;
  }

  public void setFlightId(String flightId) {
    this.flightId = flightId;
  }

  public String getFlightId() {
    return flightId;
  }

  public void setSource(String source) {
    this.source = source;
  }

  public String getSource() {
    return source;
  }

  public void setStatus(String status) {
    this.status = status;
  }

  public String getStatus() {
    return status;
  }

  @Override
  public String toString() {
    return "Route{" +
    "flightRouteId=" + flightRouteId +
    ", destination=" + destination +
    ", flightId=" + flightId +
    ", source=" + source +
    ", status=" + status +
    "}";
  }
}
