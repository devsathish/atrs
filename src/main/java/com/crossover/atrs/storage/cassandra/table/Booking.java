package com.crossover.atrs.storage.cassandra.table;

import com.crossover.atrs.storage.cassandra.udt.Ticket;
import com.datastax.driver.mapping.annotations.Column;
import com.datastax.driver.mapping.annotations.FrozenValue;
import com.datastax.driver.mapping.annotations.PartitionKey;
import com.datastax.driver.mapping.annotations.Table;
import com.fasterxml.jackson.annotation.JsonProperty;
import java.lang.Override;
import java.lang.String;
import java.util.Date;
import java.util.List;

/**
 * GENERATED CODE DO NOT MODIFY - last updated: 2016-08-07T13:03:45.787Z
 * generated by exovert - https://github.com/cyngn/exovert
 *
 * Table class for Cassandra - booking
 */
@Table(
    keyspace = "atrs",
    name = "booking"
)
public class Booking {
  @Column(name = "booking_id")
  @JsonProperty("booking_id")
  @PartitionKey(0)
  public String bookingId;

  @Column(name = "booking_time")
  @JsonProperty("booking_time")
  public Date bookingTime;

  @Column(name = "booking_user_id")
  @JsonProperty("booking_user_id")
  public String bookingUserId;

  @Column
  @JsonProperty
  public String status;

  @FrozenValue
  @Column
  @JsonProperty
  public List<Ticket> tickets;

  public void setBookingId(String bookingId) {
    this.bookingId = bookingId;
  }

  public String getBookingId() {
    return bookingId;
  }

  public void setBookingTime(Date bookingTime) {
    this.bookingTime = bookingTime;
  }

  public Date getBookingTime() {
    return bookingTime;
  }

  public void setBookingUserId(String bookingUserId) {
    this.bookingUserId = bookingUserId;
  }

  public String getBookingUserId() {
    return bookingUserId;
  }

  public void setStatus(String status) {
    this.status = status;
  }

  public String getStatus() {
    return status;
  }

  public void setTickets(List<Ticket> tickets) {
    this.tickets = tickets;
  }

  public List<Ticket> getTickets() {
    return tickets;
  }

  @Override
  public String toString() {
    return "Booking{" +
    "bookingId=" + bookingId +
    ", bookingTime=" + bookingTime +
    ", bookingUserId=" + bookingUserId +
    ", status=" + status +
    ", tickets=" + tickets +
    "}";
  }
}
