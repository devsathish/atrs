package com.crossover.atrs.storage.cassandra.dal;

import com.cyngn.vertx.async.ResultContext;
import java.lang.Object;
import java.util.function.Consumer;

/**
 * GENERATED CODE DO NOT MODIFY - last updated: 2016-08-07T13:03:45.787Z
 * generated by exovert - https://github.com/cyngn/exovert
 *
 * Common interface for all DAL classes
 */
public interface CommonDal<T> {
  void save(final T entity, final Consumer<ResultContext> onComplete);

  void get(final Consumer<ResultContext<T>> onComplete, final Object... primaryKeys);

  void delete(final Consumer<ResultContext> onComplete, final Object... primaryKeys);

  void delete(final T entity, final Consumer<ResultContext> onComplete);
}
