angular.module('ATRS').controller('SearchCtrl', ['$scope', '$rootScope', '$location', '$route', '$routeParams', '$location', 'AtrsSvc',
    function($scope, $rootScope, $location, $route, $routeParams, $location, atrsSvc) {
        $scope.search = function() {
            var params = {};
            atrsSvc.search(params).then(function(data) {
                $scope.results = data.results;
            });
        }

        $scope.book = function(flight) {
            var userId = 'Sathish';
            var meal = 'VEGETERIAN';
            var seatId = atrsSvc.rand(2, 98)+'B';
            var params = {
                booking_id: atrsSvc.hash(),
                user_id: userId,
                ticket_info: [
                    {
                        ticketId: atrsSvc.hash(),
                        flightRouteId: flight.flight_route_id,
                        flightTripId: flight.flight_trip_id,
                        seatId: seatId,
                        passengerId: userId,
                        mealPreference: meal
                    }
                ]
            }

            atrsSvc.book(params).then(function(response) {
                console.log('Booking: ', response);
                $location.url('bookings');
            });
        }
    }
]);