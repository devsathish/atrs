angular.module('ATRS').controller('ListBookingsCtrl', ['$scope', '$rootScope', '$location', '$route', '$routeParams', '$location', 'AtrsSvc',
    function($scope, $rootScope, $location, $route, $routeParams, $location, atrsSvc) {
        //TODO: Temporary util method to parse the search result and get the flight details. Replace it with flights API.
        var getFlight = function(sResults, id) {
            var r = sResults.results;
            for(var i=0, max = r.length; i<max; i++) {
                var flight = r[i];
                if(flight.flight_trip_id == id) {
                    return flight;
                }
            }
        }

        $scope.cancel = function(bookingId) {
            atrsSvc.cancelBooking(bookingId).then(function() {
                alert('Cancellation of booking id '+bookingId+' was successful.');
                $location.url('bookings');
            });
        }

        atrsSvc.getAllBookings().then(function(bookings) {
            atrsSvc.search({}).then(function(sResults) {
                for(var i =0, iMax=bookings.length; i<iMax; i++) {
                    var booking = bookings[i];
                    for(var j =0, jMax=booking.tickets.length; j < jMax; j++) {
                        var ticket = booking.tickets[j];
                        var flight = getFlight(sResults, ticket.flight_trip_id);
                        ticket.flight = flight;
                    }
                }
                $scope.results = bookings;
            });
        });
    }
]);