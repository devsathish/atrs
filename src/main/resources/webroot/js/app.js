var portal = angular.module('ATRS',
    ['ngRoute', 'ui.bootstrap']);


portal.config(['$routeProvider', '$locationProvider',
    function($routeProvider, $locationProvider) {
        var p = '/s/partials/';
        $routeProvider
        .when('/login', {
            templateUrl: p+'login.html',
            controller: 'LoginCtrl'
        })
        .when('/search', {
            templateUrl: p+'search.html',
            controller: 'SearchCtrl'
        })
        .when('/bookings/', {
            templateUrl: p+'list_bookings.html',
            controller: 'ListBookingsCtrl'
        })
        .when('/booking/:bookingId', {
            templateUrl: p+'booking_details.html',
            controller: 'BookingDetailsCtrl'
        })
        .otherwise({
            redirectTo: '/search'
        });
}]);