angular.module('ATRS').factory('AtrsSvc', ['$q', '$http', '$timeout', function($q, $http, $timeout) {
    var svc = {};

    // Utility method to make http calls.
    var http = function(opt) {
        var deferred = $q.defer();
        var url = '/' + opt.path;
        var config = {
             method: opt.method || 'GET',
             url: url,
             timeout: opt.timeout || 120000
        };
        var key = 'data';
        if(config.method.toLowerCase() == 'get' || config.method.toLowerCase() == 'delete') {
            key = 'params';
        }
        config[key] = opt.data;
        $http(config).then(function(response) {
            console.log('Response from server: ', response.data);
            deferred.resolve(response.data);
        }, function(e) {
            if ( e.status <=0 ) {
                e.statusText = "Unable to connect to server, HttpConnection:Error";
                console.log("Error, not able connect ", endPoint, e );
                deferred.reject(e);
            } else {

                console.log('Error occured while processing your request to URL: '+
                    url, ', Params given: ', opt, e);
                deferred.reject(e);
            }
        });
        return deferred.promise;
    }
    // Book flight ticket.
    svc.book = function(data) {
        return http({
            method: 'post',
            path: 'api/book',
            data: data
        });
    }

    // Get details of a booking.
    svc.getBooking = function(data) {
        return http({
            path: 'api/booking',
            data: data
        });
    }

    // Get details of all bookings.
    svc.getAllBookings = function(data) {
        return http({
            path: 'api/bookings',
            data: data
        });
    }

    // Get details of all bookings.
    svc.cancelBooking = function(bookingId) {
        return http({
            method: 'POST',
            path: 'api/booking/cancel',
            data: {booking_id: bookingId}
        });
    }


    // Search and return dummy data.
    svc.search = function(params) {
        return http({
            path: 's/dummy/search-results.json',
            data: params
        });
    }

    // Generate random hash till the given number.
    svc.hash = function(max) {
        var allowed = "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789";
        var res = [];
        var m = max || 5;
        for( var i=0; i < m; i++ ) {
            res.push(allowed.charAt(Math.floor(Math.random() * allowed.length)));
        }
        return res.join('');
    }

    // Generate random number between given range.
    svc.rand = function(min, max) {
        return parseInt(Math.random() * (max - min) + min, 10);
    }
    return svc;
}]);