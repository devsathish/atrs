// ATRS Cassandra Schema Script
CREATE KEYSPACE atrs WITH replication = {'class':'SimpleStrategy', 'replication_factor' : 1};

/*
 * Table to hold details of each available route (Ex: flight from London to Paris on 15th Aug will have an entry here.)
 */

CREATE TYPE IF NOT EXISTS atrs.ticket (
    ticket_id varchar,
    seat_id varchar,
    passenger_id varchar,
    booking_id varchar,
    ticket_status varchar,
    meal_preference varchar,
    flight_route_id varchar,
    flight_trip_id varchar,
    checkin_status varchar,
    boarding_pass_status varchar
);

CREATE TABLE IF NOT EXISTS atrs.booking (
    booking_id varchar,
    booking_user_id varchar,
    tickets frozen<list<ticket>>,
    booking_time timestamp,
    status varchar,
    PRIMARY KEY(booking_id)
);

// TBD for other tables.