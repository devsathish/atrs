#  Air Ticket Reservation System #

## Overview ##
The Air Ticket Reservation System (ATRS) is designed to search, book and manage the flight tickets.

## About this Package ##
This repository contains source code of all files to run Air Ticket Reservation System (ATRS).

## What has been implemented? ##
* The backend service (APIs to book ticket, get list of all bookings, get booking details from its id, cancel booking). 
* Cassandra Storage Setup
* Single Page Application to manage the details


## Technology Stack ##
### Server Side ###
The server side deals with lot of IO connections and hence I recommend to use and event driven server implementation for this ATRS application.
We use Vertx.io, which is a toolkit to build reactive applications. Vertx is event driven and non blocking. More details about Vertx framework is available [here][vertxio].
Managing configuration and injecting dependencies are done through [Guice][guice] (Google's dependency injection package).

### Storage ###
Given that ticket reservation system has high usage, ATRS requires high throughput to process and response to each queries should be quicker.
Traditional RDBMS won't scale horizontally. Hence I chose NoSQL solution for the storage. There are many solutions available (like AWS DynamoDB, Apache Cassandra).
I chose [Apache Cassandra][cassandra] for our usecase.

### Front End ###
We use [AngularJS][angularjs] to render the UI. The complete interface is Single Page App (SPA).

## Dev Setup ##
### Build Tools###
I used Gradle to build this project. Its easy and has lot more advatanges to build java projects.
Install IntelliJ IDEA or eclipse IDE for integrated build process.
I used [Exovert][exovert] source code generator (i.e., its similar to RAML/Swagger. We write API definition in json file and it converts API abstract classes). Also, Exovert helps to generate Data Access Layers (DAL). I chose Exovert because it has better support for Vertx and Cassandra.

[Download][cassandra-download] Cassandra and extract it into a folder. Start it by running `sudo ./cassandra` and access cassandra shell by running `./cqlsh` command.

To setup database, run all sql statements from `atrs_setup.sql` .

### Folder Structure ###
~~~~
atrs
|
|- model (All API configuration).
|
|- database (SQL files to setup the database, schema and tables)
|
|- src
    |
    |- main
        |
        |- java (All Java files for the project. Contains generated + handwritten source files).
        |
        |- resources (All static files - HTML / CSS / JavaScript / Fonts) 
~~~~

## Usage ##

To start the server run the following code,

~~~~ 
vertx run -conf src/main/resources/config.json com.crossover.atrs.service.AtrsServer
~~~~

More details about starting server/verticle, is [here][start].

## Contact ##

To know more about this prioject, please contact [our team][email].

[vertxio]:<http://vertx.io/>
[angularjs]:<https://angularjs.org/>
[guice]:<https://github.com/qrman/vertx-guice>
[cassandra]:<http://cassandra.apache.org/>
[cassandra-download]:<http://cassandra.apache.org/download/>
[start]:<http://vertx.io/docs/vertx-core/java/#_running_verticles_from_the_command_line>
[exovert]:<https://github.com/cyngn/exovert>
[email]:<mailto:abc@example.com>